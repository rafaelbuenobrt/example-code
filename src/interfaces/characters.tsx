interface IThumbnail {
  path: string;
  extension: string;
}

interface IComicSummary {
  resourceURI: string;
  name: string;
}

interface ISeriesSummary {
  resourceURI: string;
  name: string;
}

interface IStorySummary {
  resourceURI: string;
  name: string;
  type: string;
}

interface IEventSummary {
  resourceURI: string;
  name: string;
}

interface IComics {
  available: number;
  returned: number;
  collectionURI: string;
  items: IComicSummary[];
}

interface ISeries {
  available: number;
  returned: number;
  collectionURI: string;
  items: ISeriesSummary[];
}

interface IStories {
  available: number;
  returned: number;
  collectionURI: string;
  items: IStorySummary[];
}

interface IEvents {
  available: number;
  returned: number;
  collectionURI: string;
  items: IEventSummary[];
}

interface IUrls {
  type: string;
  url: string;
}

export interface ICharacter {
  id: number;
  name: string;
  description: string;
  modified: Date;
  thumbnail: IThumbnail;
  resourceURI: string;
  comics: IComics;
  series: ISeries;
  stories: IStories;
  events: IEvents;
  urls: IUrls[];
}
