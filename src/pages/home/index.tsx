import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import SearchBar from 'material-ui-search-bar';
import { Pagination } from '@material-ui/lab';
import CharacterTable from '../../components/character-table';
import { ICharacter } from '../../interfaces/characters';
import './styles.css';
import { getCharacters } from '../../utils/requests';

const Home: React.FC = () => {
  const [searched, setSearched] = useState<string>('');
  const [page, setPage] = React.useState(1);
  const [rowsPerPage] = React.useState(4);
  const [charactersFull, setCharactersFull] = useState<ICharacter[]>();
  const [characters, setCharacters] = useState<ICharacter[]>();

  const requestSearch = (searchedVal: string) => {
    setPage(1);
    if (searchedVal !== '') {
      setCharacters(
        charactersFull!
          .filter(
            (character) => character.name.toLowerCase().includes(searchedVal.toLocaleLowerCase()),
          ),
      );
    } else {
      setCharacters(charactersFull);
    }
    setSearched(searchedVal);
  };

  const cancelSearch = () => {
    setSearched('');
    requestSearch('');
  };

  const handlePagination = (event: any, val: number) => {
    setPage(val);
  };

  useEffect(() => {
    if (!characters) {
      (async () => {
        const getCharactersList = await getCharacters();
        setCharacters(getCharactersList);
        setCharactersFull(getCharactersList);
      })();
    }
  }, [characters]);

  return (
    <div data-testid="homepage">
      {characters && (
        <Grid
          container
        >
          <Grid
            item
            xs={1}
          />
          <Grid
            item
            xs={10}
          >
            <Typography
              component="h4"
              variant="h4"
              className="seekCharacterText"
            >
              <strong>
                Busca de personagens
              </strong>
            </Typography>
            <Typography
              component="p"
              className="characterNameText"
            >
              <strong>
                Nome do personagem
              </strong>
            </Typography>
            <SearchBar
              className="searchBar"
              value={searched}
              onChange={(searchVal) => requestSearch(searchVal)}
              onCancelSearch={() => cancelSearch()}
            />
            <CharacterTable
              characters={characters}
              page={page}
              rowsPerPage={rowsPerPage}
            />
          </Grid>
          <Grid item xs={1} />
          <div className="footer">
            <Pagination
              count={Math.ceil(characters.length / rowsPerPage)}
              color="primary"
              page={page}
              onChange={handlePagination}
              showFirstButton
              showLastButton
              shape="rounded"
            />
          </div>
        </Grid>
      )}
    </div>
  );
};

export default Home;
