import React from 'react';
import { render } from '@testing-library/react';
import Home from '../../components/routes/index';

describe('Home Component', () => {
  it('Renderizar texto home page', async () => {
    const { getByTestId } = render(<Home />);
    expect(getByTestId('homepage')).toBeInTheDocument();
  });
});
