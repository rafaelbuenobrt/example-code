import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import { getCharacterById } from '../../utils/requests';
import { ICharacter } from '../../interfaces/characters';
// import charactersMock, { ICharacter } from '../../mocks/characters';

interface RouteParams {
  id: string
}

interface CharacterProps extends RouteComponentProps<RouteParams> {
}

const useStyles = makeStyles({
  root: {
    maxWidth: 350,
  },
  media: {
    height: 200, // as an example I am modifying width and height
    width: '100%',
  },
});

const Character: React.FC<CharacterProps> = (props) => {
  const [character, setCharacter] = useState<ICharacter>();
  const classes = useStyles();

  useEffect(() => {
    (async () => {
      const getCharacter = await getCharacterById(+props.match.params.id);
      setCharacter(getCharacter);
    })();
  }, []);

  return (
    <div>
      {character && (
        <Grid
          container
        >
          <Grid
            item
            xs={1}
          />
          <Grid
            item
            container
            justifyContent="center"
            alignItems="center"
            xs={10}
            style={{ height: '80vh' }}
          >
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image={`${character.thumbnail.path}.${character.thumbnail.extension}`}
                  title="Contemplative Reptile"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {character.name}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    <strong>Eventos:</strong>
                    {' '}
                    {character.events.items.slice(0, 5).map((event) => event.name).join(', ')}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    <strong>Séries:</strong>
                    {' '}
                    {character.series.items.slice(0, 5).map((event) => event.name).join(', ')}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary" onClick={() => { props.history.push('/'); }}>
                  Voltar
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={1} />
        </Grid>
      )}
    </div>
  );
};

export default Character;
