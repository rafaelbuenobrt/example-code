import axios from 'axios';
import md5 from 'crypto-js/md5';
import { ICharacter } from '../interfaces/characters';

interface MarvelApi {
  publicKey: string;
  privateKey: string;
  ts: number;
  md5: string;
}

const apiKeys = (): MarvelApi => {
  const publicKey = String(process.env.REACT_APP_MARVEL_PUBLIC_KEY) || '';
  const privateKey = String(process.env.REACT_APP_MARVEL_PRIVATE_KEY) || '';
  const ts = Number(process.env.REACT_APP_MARVEL_TS) || 1;

  return {
    publicKey,
    privateKey,
    ts,
    md5: md5(ts + privateKey + publicKey).toString(),
  };
};

const getCharacters = async (): Promise<ICharacter[]> => {
  const keys: MarvelApi = apiKeys();

  const response = await axios.get(
    `http://gateway.marvel.com/v1/public/characters?ts=${keys.ts}&apikey=${keys.publicKey}&hash=${keys.md5}`,
  );

  return response.data.data.results;
};

const getCharacterById = async (id: number): Promise<ICharacter> => {
  const keys: MarvelApi = apiKeys();

  const response = await axios.get(
    `http://gateway.marvel.com/v1/public/characters/${id}?ts=${keys.ts}&apikey=${keys.publicKey}&hash=${keys.md5}`,
  );

  return response.data.data.results[0];
};

export {
  getCharacters,
  getCharacterById,
};
