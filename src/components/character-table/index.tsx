import React, { useEffect } from 'react';
import {
  withStyles, createStyles, makeStyles, Theme,
} from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
// import { Link } from 'react-router-dom';
import { ICharacter } from '../../interfaces/characters';

const StyledTableCell = withStyles((theme: Theme) => createStyles({
  head: {
    backgroundColor: 'none !important',
    color: '#969696',
  },
  body: {
    fontSize: 14,
    [theme.breakpoints.down('sm')]: {
      padding: 2,
    },
    padding: 4,
    cursor: 'pointer',
  },
}))(TableCell);

const useStyles = makeStyles((theme: Theme) => ({
  table: {
    [theme.breakpoints.down('sm')]: {
      minWidth: 1300,
    },
    borderCollapse: 'separate',
    borderSpacing: '0px 6px',
  },
  tableContainer: {
    boxShadow: 'none',
    backgroundColor: '#E5E5E5',
    padding: '2px',
  },
  tableHead: {
    backgroundColor: '#E5E5E5',
  },
  tableCellFlex: {
    display: 'flex',
    alignItems: 'center',
  },
  tableRow: {
    backgroundColor: '#FFFFFF !important',
    boxShadow: '0px 1px 4px 0px rgba(0,0,0,0.5)',
    '&:hover': {
      boxShadow: '0px 1px 4px 0px rgba(0,0,0,0.8)',
    },
    color: 'inherit',
    textDecoration: 'inherit',
  },
}));

interface RouteParams {}
interface CharacterTableProps extends RouteComponentProps<RouteParams> {
  characters: ICharacter[]
  page: number
  rowsPerPage: number
}

const CharacterTable: React.FC<CharacterTableProps> = ({
  characters,
  page,
  rowsPerPage,
  history,
}) => {
  const classes = useStyles();

  useEffect(() => {
  }, [characters, page, rowsPerPage]);

  const goToCharacter = (id: number) => {
    history.push(`/${id}`);
  };

  return (
    <TableContainer
      component={Paper}
      className={classes.tableContainer}
    >
      <Table
        className={classes.table}
        aria-label="customized table"
      >
        <TableHead className={classes.tableHead}>
          <TableRow>
            <StyledTableCell style={{ padding: '4px' }}>Personagem</StyledTableCell>
            <StyledTableCell style={{ padding: '4px' }} align="left">Séries</StyledTableCell>
            <StyledTableCell style={{ padding: '4px' }} align="left">Eventos</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? characters.slice((page - 1) * rowsPerPage, page * rowsPerPage)
            : characters
          ).map((character) => (
            <TableRow
              key={character.id}
              className={classes.tableRow}
            >
              <StyledTableCell
                component="th"
                scope="row"
                onClick={() => goToCharacter(character.id)}
              >
                <div className={classes.tableCellFlex}>
                  <img
                    alt="character"
                    src={`${character.thumbnail.path}.${character.thumbnail.extension}`}
                    width="48px"
                    height="48px"
                    style={{ marginRight: '6px' }}
                  />
                  <strong>{character.name}</strong>
                </div>
              </StyledTableCell>
              <StyledTableCell
                align="left"
                onClick={() => goToCharacter(character.id)}
              >
                {character.series.items.slice(0, 3).map((serie, index) => (
                  <div key={index}>{serie.name}</div>
                ))}
                {character.series.items.length === 0 && <div>Não há séries.</div>}
              </StyledTableCell>
              <StyledTableCell
                align="left"
                onClick={() => goToCharacter(character.id)}
              >
                {character.events.items.slice(0, 3).map((events, index) => (
                  <div key={index}>{events.name}</div>
                ))}
                {character.events.items.length === 0 && <div>Não há eventos.</div>}
              </StyledTableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default withRouter(CharacterTable);
