import React from 'react';
import {
  Route,
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom';
import Home from '../../pages/home';
import Character from '../../pages/character';
import Header from '../header';

const Routes: React.FC = () => (
  <Router>
    <Header />
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/:id" component={Character} />
    </Switch>
  </Router>
);

export default Routes;
