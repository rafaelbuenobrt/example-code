import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import Header from './index';

describe('Header Component', () => {
  it('Renderizar imagem de logo', () => {
    render(
      <BrowserRouter>
        <Header />
      </BrowserRouter>,
    );
    const buttonElement = screen.getByTestId('logo');
    expect(buttonElement).toHaveAttribute('h1');
    expect(buttonElement).toBeInTheDocument();
  });

  it('Renderizar nome', () => {
    render(
      <BrowserRouter>
        <Header />
      </BrowserRouter>,
    );
    const name = screen.getByText(/rafael bueno/i);
    expect(name).toBeInTheDocument();
  });

  it('Renderizar código de front-end', () => {
    render(
      <BrowserRouter>
        <Header />
      </BrowserRouter>,
    );
    const name = screen.getByText(/código de exemplo/i);
    expect(name).toBeInTheDocument();
  });
});
