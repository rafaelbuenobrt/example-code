import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import './styles.css';

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    backgroundColor: 'white',
    color: '#575757',
    boxShadow: '0px 1px 5px -3px #000000',
    marginBottom: '4vh',
  },
}));

const Header: React.FC = () => {
  const classes = useStyles();

  return (
    <AppBar
      position="static"
      className={classes.root}
    >
      <Toolbar>
        <Grid
          container
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid
            item
            xs={2}
          >
            <Link to="/" style={{ textDecoration: 'none', color: 'inherit' }}>
              <h1>Rafael Bueno</h1>
            </Link>
          </Grid>
          <Grid
            container
            item
            lg={3}
            xs={5}
            md={3}
            sm={5}
            alignItems="center"
          >
            <Grid
              item
              lg={4}
              md={5}
              sm={4}
              xs={12}
            >
              <Typography component="p">
                <strong>Rafael Bueno</strong>
              </Typography>
            </Grid>
            <Grid
              item
              lg={8}
              md={7}
              sm={8}
              xs={12}
            >
              <Typography component="p">
                Código de exemplo
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
